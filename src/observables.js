import {fromEvent, Observable, Subscription} from 'rxjs';

import {debounceTime, distinctUntilChanged, map, startWith, tap} from 'rxjs/operators';

let onDestroy = true

let scrollDebounce = 250


const windowResize = fromEvent(window, 'resize').pipe(takeUntil(onDestroy))

const windowScroll = fromEvent(window, 'scroll').pipe(debounceTime(scrollDebounce), 
                                                        takeUntil(onDestroy))
/**
 * Returns a subscription. To stop listening run subscription.unsubscribe()
 * @param {()=>void} observer 
 */
export const subscribeResize = (observer)=> observer && windowResize.subscribe(observer)

/**
 * Returns a subscription. To stop listening run subscription.unsubscribe()
 * @param {()=>void} observer 
 */
export const subscribeScroll = (observer)=> observer && windowScroll.subscribe(observer)
